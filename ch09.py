# Exercise: factoring selection sort
# Objective: Factor out the inner loop of a while loop.

# Factoring code is the process by which common operations are recognized and moved into functions, to increase readability and the possibility of re-use of code. In selection sort, the inner loop finds the smallest value in the list, starting at some index. Write a function find_min_in_sublist(list, start) that returns the index of the smallest value in a list at or after index start. Then use a call to that function to replace the inner loop of selection sort.)

def find_min_in_sublist(list, start):
    smallest = start
    for j in range(start, len(list)):
        if list[j] < list[smallest]:
            smallest = j
    return smallest

def selection_sort(the_list):
    n = len(the_list)     # makes it easy to denote the length of the list

    for i in range(n - 1):
        smallest = find_min_in_sublist(the_list, i)

        # Swap the values at index i and smallest.
        temp = the_list[i]
        the_list[i] = the_list[smallest]
        the_list[smallest] = temp

grade_list = [89, 45, 85, 81, 77, 94, 22, 79, 92, 91]
selection_sort(grade_list)
print("grade_list after sorting:  " + str(grade_list) )