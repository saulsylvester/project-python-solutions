# Exercise: factoring selection sort
# Objective: Factor out the inner loop of a while loop.

# Factoring code is the process by which common operations are recognized and moved into functions, to increase readability and the possibility of re-use of code. In selection sort, the inner loop finds the smallest value in the list, starting at some index. Write a function find_min_in_sublist(list, start) that returns the index of the smallest value in a list at or after index start. Then use a call to that function to replace the inner loop of selection sort.)