###Exercise 2

# Exercise: hello, hello!
# Objective: Write and call your own functions to learn how the program counter steps through code.

# You will write a program that introduces you and prints your favorite number.

# Write the body of the function print_favorite_number
# Write a function call at the end of the program that calls print_favorite_number.
# Write a new function, say_introduction that prints Hello, my name is X. and I am a Y. on two separate lines, where X and Y are replaced by whatever you like.
# Add a line that calls say_introduction so that the introduction is printed before the favorite number.

def print_favorite_number():
	print("17")

print(print_favorite_number())

def say_introduction(X,Y):
	print("Hi my name is {}. I am a {}".format(X,Y))


###Exercise 2
#### BEFORE YOU START THIS, it's super annoying, but the cs1lib library doesn't work on your local machine.


# Objective: learn how to call drawing library functions that take parameters, in order to draw a desired complex picture.

# Write a program that causes a yellow smiley face to be drawn on the screen. You have a 200 by 200 window available.

# Draw the outline of the face. Put your code for drawing the outline after the line # draw the outline of the face. (Lines that begin with # are ignored by the computer, and are for human readers only.

# Draw the eyes. Put your code for drawing the eyes after the line # draw the eyes.

# Bonus challenge. Draw the mouth. Hint – draw a circle, and then erase the top part of it by drawing some sort of yellow shape.



from cs1lib import start_graphics, clear, draw_circle, draw_rectangle, set_fill_color, set_stroke_color #### Description found here: http://projectpython.net/chapter19/ BEFORE YOU START THIS, it's super annoying, but the cs1lib library doesn't work on your local machine. try 

def draw():
	# draw a white background
    clear()
    # # draw the outline of the face
    set_fill_color(1, 1, 0)   # set fill color to yellow
    draw_circle(100, 100, 50)
    # # draw the mouth
    set_fill_color(1, 1, 0)  # yellow
    draw_circle(100, 100, 30)
    set_stroke_color(1, 1, 0)
    draw_rectangle(68, 68, 62, 40)

    # draw the eyes
    set_stroke_color(0, 0, 0)
    set_fill_color(0, 0, 0)  # set fill color to black
    draw_circle(100 - 20, 100 - 10, 5)
    draw_circle(100 + 20, 100 - 10, 5)


start_graphics(draw)