# Exercise: alarm
# Objective: Use a callback to trigger a delayed call to a function you write.

# In the browser version of Python, you can import a function called alert from the module browser. Alert takes a single string as a parameters. Try calling alert now with a sample string.

# Now use set_timeout to display an alert with some text after 10 seconds. You should call set_timeout with only two parameters, since that’s the proper way to schedule a single event.

# Hint. Notice alert takes a parameter, but set_timeout only works properly with functions that do not take any parameters. So you can’t call alert directly. Maybe you could define another function that didn’t take any parameters…
