# Project Python Solutions

Solutions to Project Python exercises.

Found here: http://projectpython.net/chapter00/ Project Python is a free interactive book that will teach you to code in Python, using graphics, animations, and games. You’ll also learn ways to solve classical computer science problems, principles of software design, and how to analyze algorithm performance. 