# Exercise: writing a Queue class
# Objective: write a class that implements a data structure by wrapping a simple data type.

# There may be many different ways to implement a data type such as Queue. We define the Queue based on the operations available on the Queue, not based on how it is implemented: a Queue is a data type that supports enqueue and dequeue operations. It’s convenient to have a class that handles the details of the implementation.

# Write a Queue class that internally uses a list to keep the data in order. Your Queue class should have methods to enqueue an item, dequeue and return the item, empty which returns True if there are no more items on the Queue, and __str__ which returns a string representation of the queue.
# (Hint: none of the bodies of these methods needs to be more than one line long.)


class Queue:
    def __init__(self):
        self.items = []

    def enqueue(self, x):
        self.items.append(x)

    def dequeue(self):
        return self.items.pop(0)

    def empty(self):
        return self.items == []

    def __str__(self):
        return str(list(self.items))

q = Queue()

q.enqueue("Dave")
print("Dave got in line:  " + str(q))

q.enqueue("Eliza")
print("Eliza got in line:  " + str(q))

q.enqueue("Alice")
print("Alice got in line:  " + str(q))

next = q.dequeue()
print(next + " is served:  " + str(q))

q.enqueue("Carol")
print("Carol got in line:  " + str(q))

q.enqueue("Betty")
print("Betty got in line:  " + str(q))

next = q.dequeue()
print(next + " is served:  " + str(q))

next = q.dequeue()
print(next + " is served:  " + str(q))

next = q.dequeue()
print(next + " is served:  " + str(q))


# Exercise: count digits in a list
# Objective: Implement an efficient algorithm that counts the frequency of numbers in a list.

# You have a lot of digits stored in a list, digits. Maybe the digits are a secret code! How frequently does each digit appear? Create a new list, frequency with indices 0 through 9, and implement a linear-time algorithm such that when run, frequency[0] contains the number of times that 0 appears in digits, frequency[1] contains the number of times that 1 appears, and so forth. (Hint. You should need to loop over the digits list exactly one time.)
digits = [0, 3, 2, 3, 4, 3, 2, 2, 1, 7, 6, 4, 5, 8, 3, 2,
          1, 2, 1, 1, 1, 0, 0, 0, 2, 2, 3, 4, 5, 6, 2, 9, 8]

frequency = [0] * 10
for digit in set(digits):
    frequency[digit] = digits.count(digit)

# Exercise: words of the Constitution
# Objective: Make use of a dictionary to count frequencies.

# The following code loads the words of the Constitution of the United States into a list. Some of the words are interesting, in that they contribute to the uniqueness of the document, and others are not. For example, the word “the” appears frequently in the document, but is not interesting. Create a dictionary that contains all words of length greater than four as keys, and the frequency of each of those words as associated values.

def file_to_wordlist(filename):
    f = open(filename, 'r')
    words = []
    for line in f:
        # make lowercase
        line = line.lower()
        # strip punctuation
        line = line.replace(";", " ").replace(".", " ").replace(",", " ").replace("-", " ").replace(":", " ")
        words.extend(line.split(" "))

    return words

def print_sorted_by_frequency(frequncy):
    sorted_frequency = sorted(frequency.items(), key=lambda item: -item[1])
    for key_value in sorted_frequency:
        print(key_value)

words = file_to_wordlist("input/constitution-preamble.txt")

# You write this part to create the dictionary `frequency`:

#  Prints the words in the dictionary sorted by frequency
print_sorted_by_frequency(frequency)
