
# Exercise: linked list of even numbers
# Objective: Write a program that adds several items to a linked list using a loop.

# Write a program that adds all of the even numbers from 0 to 100 to a linked list, and then prints out all of the items in the list. To save some typing, a simple Node class is provided, as is the code to print the list. You will need to do several things to create the linked list.

# Create a first node; call it head.
# Create a variable current that will point to the node we are currently working on.
# For each data item, create a node and store a reference to it in an instance variable of current. Then update current to point to the next node in the list we are building.


class Node:
    def __init__(self, data):
        self.data = data  # instance variable to store the data
        self.next = None  # instance variable with address of next node

head = Node(0)
current = head
for i in range(1, 51):
    current.next = Node(2 * i)
    current = current.next

# Print the data of the list in order:
current = head   # copy the address of the head node into node
while current != None:
    print(current.data)
    current = current.next


# Exercise: needle in a linked list
# Objective: write a method that implements an algorithm on a simple linked list.

# Write a method find of the Node class that is passed a data item as parameter, and returns a reference to the first node in the linked list containing that item, or `None’ if the item is not found. (Use a loop to search for the item.)

class Node:
    def __init__(self, data):
        self.data = data  # instance variable to store the data
        self.next = None  # instance variable with address of next node

    def find(self, data):
        current = self
        while(current != None):
            if current.data == data:
                return current
            current = current.next

        return None

# The head is the first node in a linked list.
head = Node("Maine")
another_node = Node("Idaho")
head.next = another_node
a_third_node = Node("Utah")
another_node.next = a_third_node

print("Found:  " + str(head.find("Utah").data))