# Exercise: coin flip
# Objective: Make use of different results of a function call to take different actions.

# Write a loop that simulates flipping a coin 5 times, and prints out “heads” or “tails” after each flip. Use the Python randint function to determine if the outcome of each flip should be heads or tails.
import random
for x in range(5):
	if random.randint(1,2) % 2 == 0:
		print("heads")
	else:
		print("tails")

# Exercise: area 51
# Objective: Write a function that returns a value.

# First, write a function circle_area51 that computes the area of a circle of radius 51, and returns that area. Call the function and print the result to verify that it works.

# Then write a function circle_area that takes a parameter radius, and computes the area o a circle with that radius. Call the function three times, to compute the areas of circles of size 3, 5, and 51, and print the results.

from math import pi
def circle_area51(x):
	return pi*(51**2)


def circle_area(x):
	for val in [3,5,51]:
		print(pi*(val**2))