# Exercise: even in a range
# Objective: Write a boolean expression to test if a variable satisfies several conditions.

# Write a single line of code that tests if the value of the variable x is even and has a value in the range 24 and 32 (inclusive), and prints out True if so, and False otherwise. Test your code with the values 0, 3, 24, 25, 32, 33, and 34.


def ex1_ch3(value):
	if value //2 == value / 2:
		return True
	else:
		return False



output = [ex1_ch3(x) for x in [0, 3, 24, 25, 32, 33, 34]]
print(output)


###
# Exercise: count while
# Objective: Write a while loop that terminates after counting up to a certain value.

# Write a program to count to five and print the output as the counter increases.

import time
import random

x = 0
while True:
	x += 1
	print(x)
	if x == 5:
		break


# Exercise: Count down
# Objective: Write a while loop that changes a variable and stops when that variable reaches a particular value.

# Write a program that counts down from 100 to 0 backwards by twos. If you need to, write out all of the print statements by hand, look at what’s changing, and introduce a variable to allow that change using repeated lines of code. Then wrap into a while loop.

x = 100
while True:
	x -= 2
	if x < 1:
		break



# Exercise: series
# Objective: Use multiple variables in a while loop to change loop body behavior on each iteration.

# Write a program using a loop that prints first 100 numbers in the series of numbers: 1, 2, 4, 7, 11, 16, 22. Hint: The difference between the two consecutive numbers increases by one for each new number. You may need a variable to keep track of the current rate of increase.
# import copy
input_list = [1, 2, 4, 7, 11, 16, 22]
# first_list = copy.deepcopy(input_list)
while len(input_list) < 100:
	if len(input_list) < 8:
		for val in input_list:
			print(val)
	else:
		print(sum(input_list))

	input_list.append(sum(input_list))


# Exercise: optimize factors
# Objective. Modify existing code to implement a more efficient algorithm.

# The code for computing factors is inefficient. It loops from 1 to the number, but I claim it should only loop from 1 to the square root of the number, since factors come in pairs. For example, if 2 is a factor of 42, then so is 21. Re-write the following code so that it iterates fewer times through the loop, but still prints out all of the factors. Don’t forget to import the sqrt function from the math module.
number = 42

#old code
# possible_factor = 1
# while possible_factor <= number:
#     if number % possible_factor == 0:
#         print( str(possible_factor) + " is a factor of " + str(number) + "." )

#     possible_factor = possible_factor + 1
#new code
product_list = []
for i in range(1,number+1):
	if 42%i == 0:
		product_list.append(i)


print( "And {} are all the factors of ".format(", ".join(str(x) for x in product_list)) + str(number) + "." )



# Objective: use a generate-and-test pattern to write a loop with changing behavior.

# Write a program that counts from 1 to 20 and along with each number prints “Lion” if the number is odd and prints “Unicorn” if the number is even. The first four lines of output should be:

# 1 Lion
# 2 Unicorn
# 3 Lion
# 4 Unicorn


for val in range(1,20+1):
	if val%2 == 0:
		print(val,"Lion")
	else:
		print(val,"Unicorn")