# Exercise: function for reversing a list
# Objective: Take an implementation of a list algorithm, and wrap it in a function that takes the list as a parameter.

# Reversing a list seems like a useful thing to do; we already wrote the code to do it. Aliasing lets us move that code into a function. Write a function reverse_list that takes one parameter, the address of a list to reverse, and reverses that list in place. Test your function by calling reverse_list on a test list of numbers.

def reversed(x):
	return x[-1::-1]

test_list = [1, 3, 5, 7, 9, 11, 13, 15]
reverse_list = reversed(test_list)