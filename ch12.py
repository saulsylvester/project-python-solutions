# Exercise: recursive factorial
# Objective: Write a recursive function, with a base case and a recursive case.

# Write the function factorial with no loops, based on the observation above. You need to handle two cases. First, if the parameter n has the value 0, then factorial(n) should simply return 1. Otherwise, the function should compute factorial(n - 1) and make use of that result to compute and return the needed value.

# Compute n! recursively.
def factorial(n):
    if n == 0:
        return 1
    return n * factorial(n - 1)

print(factorial(5))


# Exercise: recursive Fibonacci call order
# Objective: Trace the execution of a recursive function, listing the order in which function calls are made.

# The recursion tree shows which function calls are made, but does not give the order in which function calls are made. In the text box, write out the order of function calls for fib(5). The first two calls are already written. Indent each line of text by a number of tabs equal to the depth in the recursion tree.
def fib(n):
    if n == 1 or n == 2:
        return 1    # base case
    else:
        return fib(n - 1) + fib(n - 2)

# Answer here!
# fib(5)
#   fib(4)
#     fib(3)
#       fib(2)
#       fib(1)
#     fib(2)
#   fib(3)
#     fib(2)
#     fib(1)